<?php

use Itgro\SbrfAcquiring\Interfaces\Currency;

return [
    'test' => env('SBRF_ACQUIRING_TEST', true),

    'logs' => env('SBRF_ACQUIRING_LOGS', true),

    'login' => env('SBRF_ACQUIRING_LOGIN', 'login'),

    'password' => env('SBRF_ACQUIRING_PASSWORD', 'secret'),

    'routes' => [
        'payment' => 'sbrf-acquiring.payment',

        'success' => 'sbrf-acquiring.success',

        'fail' => 'sbrf-acquiring.fail',
    ],

    'middleware' => 'web',

    'currency' => Currency::RUB,

    'console' => [
        'scheduling' => false,
    ],

    'language' => 'ru',

    'preauth_payment' => env('SBRF_ACQUIRING_PREAUTH_PAYMENT', true),
];