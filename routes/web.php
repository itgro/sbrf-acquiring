<?php

Route::group([
    'middleware' => config('sbrf-acquiring.middleware'),
    'namespace' => 'Itgro\SbrfAcquiring\Controllers\Http',
], function () {
    if(config('sbrf-acquiring.routes.payment') === 'sbrf-acquiring.payment') {
        Route::post('sbrf-acquiring/payment', 'Pages@payment')->name(config('sbrf-acquiring.routes.payment'));
    }

    if(config('sbrf-acquiring.routes.fail') === 'sbrf-acquiring.fail') {
        Route::get('sbrf-acquiring/fail', 'Pages@fail')->name(config('sbrf-acquiring.routes.fail'));
    }

    if(config('sbrf-acquiring.routes.success') === 'sbrf-acquiring.success') {
        Route::get('sbrf-acquiring/success', 'Pages@success')->name(config('sbrf-acquiring.routes.success'));
    }

    Route::get('sbrf-acquiring/callback', 'Pages@callback')->name('callback');
});
