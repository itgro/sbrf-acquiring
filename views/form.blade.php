<?php
/** @var \App\Models\Order $order */
?>
<form action="{{ route('sbrf-acquiring.payment') }}" method="post" class="float-left" target="_blank" style="padding-right: 10px">
    @csrf

    <input type="hidden" name="order_id" value="{{ $order->id }}"/>

    <input type="hidden" name="amount" value="{{ $order->total_amount }}"/>

    <button type="submit" class="btn btn-primary">Сбербанк</button>
</form>
