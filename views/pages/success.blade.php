@extends('sbrf-acquiring::layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Успешно!</div>
                    <div class="card-body">
                        <div class="alert alert-primary" role="alert">Оплата произведена успешно</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
