@extends('sbrf-acquiring::layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Ошибка!</div>
                    <div class="card-body">
                        <div class="alert alert-danger" role="alert">Не удалось произвести оплату, попробуйте позже</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection