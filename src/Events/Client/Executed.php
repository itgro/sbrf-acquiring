<?php

namespace Itgro\SbrfAcquiring\Events\Client;

use Itgro\SbrfAcquiring\Models\SbrfAcquiringLog;

class Executed extends ExecuteBase
{
    public function __construct($method, $request, $response)
    {
        $this->type = SbrfAcquiringLog::LOG_TYPE_INFO;

        $this->method = $method;

        $this->request = $request;

        $this->response = $response;

    }
}
