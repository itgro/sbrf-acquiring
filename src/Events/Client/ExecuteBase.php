<?php

namespace Itgro\SbrfAcquiring\Events\Client;

class ExecuteBase
{
    public $type;

    public $method;

    public $request;

    public $response;

    public $stacktrace = null;

    public $message = null;
}
