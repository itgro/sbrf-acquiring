<?php

namespace Itgro\SbrfAcquiring\Events\Client;

use Itgro\SbrfAcquiring\Models\SbrfAcquiringLog;

class ExecuteError
{
    public function __construct($method, $request, $response, $message = null, $stacktrace = null)
    {
        $this->type = SbrfAcquiringLog::LOG_TYPE_ERROR;

        $this->method = $method;

        $this->request = $request;

        $this->response = $response;

        $this->message = $message;

        $this->stacktrace = $stacktrace;
    }
}
