<?php

namespace Itgro\SbrfAcquiring\Events;

use Illuminate\Queue\SerializesModels;

class OrderCreateSuccess
{
    use SerializesModels;

    public $external_id;

    public $amount;

    public $response;

    public $metadata;

    public function __construct($external_id, $amount, $response, $metadata)
    {
        $this->external_id = $external_id;

        $this->amount = $amount;

        $this->response = $response;

        $this->metadata = $metadata;
    }
}
