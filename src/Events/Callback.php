<?php

namespace Itgro\SbrfAcquiring\Events;

use Itgro\SbrfAcquiring\Models\SbrfAcquiringOrder;

class Callback
{
    public $order;

    public $operation;

    public function __construct(?SbrfAcquiringOrder $order, $operation)
    {
        $this->order = $order;

        $this->operation = $operation;
    }
}
