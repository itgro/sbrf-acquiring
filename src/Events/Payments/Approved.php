<?php

namespace Itgro\SbrfAcquiring\Events\Payments;

use Illuminate\Queue\SerializesModels;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringOrder;

class Approved
{
    use SerializesModels;

    public $order;

    public function __construct(SbrfAcquiringOrder $order)
    {
        $this->order = $order;
    }
}