<?php

namespace Itgro\SbrfAcquiring\Events;

class GateError
{
    public $external_id;

    public $errorNo;

    public $message;

    public function __construct($external_id, $errorNo, $message)
    {
        $this->external_id = $external_id;

        $this->errorNo = $errorNo;

        $this->message = $message;
    }
}