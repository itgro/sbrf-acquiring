<?php

namespace Itgro\SbrfAcquiring\Listeners\Client;

use Itgro\SbrfAcquiring\Events\Client\ExecuteBase as Event;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringLog;

class ExecuteLog
{
    public function handle(Event $event)
    {
        $log = new SbrfAcquiringLog();

        $log->type = $event->type;
        $log->method = $event->method;
        $log->request = $event->request;
        $log->response = $event->response;
        $log->message = $event->message;
        $log->stacktrace = $event->stacktrace;

        $log->save();
    }
}
