<?php

namespace Itgro\SbrfAcquiring\Listeners\Payment;

use Itgro\SbrfAcquiring\Client\Client;
use Itgro\SbrfAcquiring\Events\Payments\CheckState as Event;

class CheckState
{
    public function handle(Event $event)
    {
        $check = resolve(Client::class)->getStateOrder($event->order->external_id);

        $event->order->status = $check->OrderStatus;

        $event->order->save();

        switch ($event->order->status) {

        }
    }
}
