<?php

namespace Itgro\SbrfAcquiring\Listeners\Order;

use Itgro\SbrfAcquiring\Events\OrderCreateSuccess;
use Itgro\SbrfAcquiring\Interfaces\OrderStatuses;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringOrder;

class Created
{
    public function handle(OrderCreateSuccess $event)
    {
        $order = new SbrfAcquiringOrder;

        $order->order_id = $event->external_id;
        $order->external_id = $event->response->orderId;
        $order->amount = $event->amount;
        $order->status = OrderStatuses::STATUS_CREATED;
        $order->metadata = $event->metadata;

        $order->save();
    }
}
