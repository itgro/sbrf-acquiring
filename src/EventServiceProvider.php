<?php

namespace Itgro\SbrfAcquiring;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Itgro\SbrfAcquiring\Events\Client\{Executed as ClientExecutedEvent, ExecuteError as ClientExecuteErrorEvent,};
use Itgro\SbrfAcquiring\Events\OrderCreateSuccess;
use Itgro\SbrfAcquiring\Events\OrderCreateSuccess as OrderCreateSuccessEvent;
use Itgro\SbrfAcquiring\Events\Payments\{Approved as PaymentApprovedEvent,
    Declined as PaymentDeclinedEvent,
    Deposited as PaymentDepositedEvent,
    Refunded as PaymentRefundedEvent,
    Reversed as PaymentReversedEvent,};
use Itgro\SbrfAcquiring\Listeners\Client\{Executed as ClientExecutedListener, ExecuteError as ClientExecuteErrorListener, ExecuteLog as ClientExecuteLogListener,};
use Itgro\SbrfAcquiring\Listeners\Order\Created as OrderCreatedListener;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringOrder;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        ClientExecutedEvent::class => [
            ClientExecutedListener::class,
            ClientExecuteLogListener::class,
        ],
        ClientExecuteErrorEvent::class => [
            ClientExecuteErrorListener::class,
            ClientExecuteLogListener::class,
        ],

        OrderCreateSuccessEvent::class => [
            OrderCreatedListener::class,
        ],
    ];

    private $listenPaymentState = [
        SbrfAcquiringOrder::STATUS_APPROVED => PaymentApprovedEvent::class,
        SbrfAcquiringOrder::STATUS_DEPOSITED => PaymentDepositedEvent::class,
        SbrfAcquiringOrder::STATUS_REVERSED => PaymentReversedEvent::class,
        SbrfAcquiringOrder::STATUS_REFUNDED => PaymentRefundedEvent::class,
        SbrfAcquiringOrder::STATUS_DECLINED => PaymentDeclinedEvent::class,
    ];

    public function boot()
    {
        parent::boot();

        $this->registerListenPaymentState();
    }

    private function registerListenPaymentState()
    {
        SbrfAcquiringOrder::saving(function (SbrfAcquiringOrder $order) {
            if ($order->status === $order->getOriginal('status')) {
                return;
            }

            $event = array_get($this->listenPaymentState, $order->status);

            if ($event) {
                event(new $event($order));
            }
        });
    }

}
