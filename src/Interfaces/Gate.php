<?php

namespace Itgro\SbrfAcquiring\Interfaces;

interface Gate
{
    const API_URL_TEST = 'https://3dsec.sberbank.ru/payment/rest/';

    const API_URL_PROD = 'https://securepayments.sberbank.ru/payment/rest/';
}