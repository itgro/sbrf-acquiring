<?php

namespace Itgro\SbrfAcquiring\Interfaces;

interface Currency
{
    const RUB = 643;
    const EUR = 978;
    const USD = 840;
}
