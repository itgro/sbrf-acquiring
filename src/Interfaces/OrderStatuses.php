<?php

namespace Itgro\SbrfAcquiring\Interfaces;


interface OrderStatuses
{
    const STATUS_BY_INTEGER = [
        self::STATUS_CREATED,
        self::STATUS_APPROVED,
        self::STATUS_DEPOSITED,
        self::STATUS_REVERSED,
        self::STATUS_REFUNDED,
        self::STATUS_AUTHORIZATION_INITIALIZED,
        self::STATUS_DECLINED,
    ];

    const STATUS_NEW = 'new';

    const STATUS_UNDEFINED = 'undefined';

    const STATUS_CREATED = 'created';

    const STATUS_APPROVED = 'approved';

    const STATUS_DEPOSITED = 'deposited';

    const STATUS_REVERSED = 'reversed';

    const STATUS_REFUNDED = 'refunded';

    const STATUS_AUTHORIZATION_INITIALIZED = 'authorization_initialized';

    const STATUS_DECLINED = 'declined';

    public function setStatusAttribute($status);

    public function getStatusByIndex($index);
}
