<?php

namespace Itgro\SbrfAcquiring\Client;

use Itgro\SbrfAcquiring\Events\Client\Executed as ClientExecutedEvent;
use Itgro\SbrfAcquiring\Events\Client\ExecuteError as ClientExecutedErrorEvent;
use Itgro\SbrfAcquiring\Events\GateError as GateErrorEvent;
use Itgro\SbrfAcquiring\Events\OrderCreateFail;
use Itgro\SbrfAcquiring\Events\OrderCreateSuccess;
use Itgro\SbrfAcquiring\Exceptions\ClientExecuteError;
use Itgro\SbrfAcquiring\Exceptions\GateSystemError;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class Client
{
    private $login;

    private $password;

    private $client;

    private $options = [];

    public function __construct(GuzzleAdapter $client, $login, $password)
    {
        $this->client = $client;
        $this->login = $login;
        $this->password = $password;
    }

    public function registerOrder($amount, $orderId = null, $parameters = [], $metadata = [], $method = 'register.do')
    {
        if (!$orderId) {
            try {
                $uuid = Uuid::uuid4();

                $orderId = $uuid->getHex();
            } catch (UnsatisfiedDependencyException $e) {
                $orderId = str_random(32);
            }
        }

        $this->setOption('orderNumber', $orderId)
            ->setOption('amount', $amount);

        if (array_has($parameters, 'jsonParams')) {
            $parameters['jsonParams'] = json_encode($parameters['jsonParams']);
        }

        if ($parameters) {
            $this->setOptions($parameters);
        }

        $this->setOption('returnUrl', route(config('sbrf-acquiring.routes.success')));
        $this->setOption('failUrl', route(config('sbrf-acquiring.routes.fail')));

        $result = $this->execute($method);

        if (isset($result->errorCode) && $result->errorCode !== 0) {
            event(new OrderCreateFail($orderId, $result, $metadata));

            //TODO: обработать ошибки
            switch ($result->errorCode) {
                default:
                    throw new GateSystemError($result->errorMessage);
                    break;
            }
        }

        event(new OrderCreateSuccess($orderId, $amount, $result, $metadata));

        return $result;
    }

    public function registerPreauthOrder($orderId, $amount, $parameters = [], $metadata = [])
    {
        return $this->registerOrder($orderId, $amount, $parameters, $metadata, 'registerPreAuth.do');
    }

    public function getStateOrder(string $order_id)
    {
        $this->setOption('orderId', $order_id);

        $result = $this->execute('getOrderStatus.do');

        if (isset($result->ErrorCode) && $result->ErrorCode != 0) {
            event(new GateErrorEvent($order_id, $result->ErrorCode, $result->ErrorMessage));
        }

        return $result;
    }

    public function getExtendStateOrder(string $order_id)
    {
        $this->setOption('orderId', $order_id);

        return $this->execute('getOrderStatusExtended.do');
    }

    public function acceptOrder(string $order_id, int $amount)
    {
        $this->setOption('orderId', $order_id)
            ->setOption('amount', $amount);

        return $this->execute('deposit.do');
    }

    public function reversalOrder(string $order_id)
    {

    }

    public function refundOrder(string $order_id)
    {

    }

    public function setOptions($parameters)
    {
        if (!is_array($parameters)) {
            throw new \InvalidArgumentException();
        }

        foreach ($parameters as $key => $value) {
            $this->setOption($key, $value);
        }
    }

    public function setOption($key, $value)
    {
        array_set($this->options, $key, $value);

        return $this;
    }

    private function execute($uri)
    {
        $this->setOption('userName', $this->login)
            ->setOption('password', $this->password)
            ->setOption('language', config('sbrf-acquiring.language'));

        try {
            $response = $this->client->request($uri, 'POST', $this->options);
        } catch (ClientExecuteError $e) {
            $message = $e->getMessage() . ' in line ' . $e->getLine();

            $trace = implode("\r\n", $e->getTrace());

            event(new ClientExecutedErrorEvent($uri, $this->options, [], $message, $trace));
        }

        event(new ClientExecutedEvent($uri, $this->options, $response));

        return $response['result'];
    }
}
