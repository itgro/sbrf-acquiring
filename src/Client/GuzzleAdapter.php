<?php

namespace Itgro\SbrfAcquiring\Client;

use GuzzleHttp\Client as GuzzleClient;

class GuzzleAdapter
{
    const METHOD_GET = 'GET';

    const METHOD_POST = 'POST';

    private $client;

    public function __construct(GuzzleClient $client)
    {
        $this->client = $client;
    }

    public function request(string $uri, string $method = self::METHOD_GET, array $data = [], array $headers = []): array
    {
        $guzzleVersion = (int)$this->client::VERSION;

        $options = ['headers' => $headers];

        if (!array_has($options, 'headers.Accept')) {
            array_set($options, 'headers.Accept', 'application/json');
        }

        if (!array_has($options, 'http_errors')) {
            array_set($options, 'http_errors', false);
        }

        switch ($method) {
            case self::METHOD_GET:
                $options['query'] = $data;
                break;

            case self::METHOD_POST:
                $options['form_params'] = $data;
                break;

            default:
                throw new \InvalidArgumentException(
                    sprintf(
                        'Invalid HTTP method "%s". Use "%s" or "%s".',
                        $method,
                        self::METHOD_GET,
                        self::METHOD_POST
                    )
                );
                break;
        }

        $response = $this->client->request($method, $uri, $options);

        $statusCode = $response->getStatusCode();

        $body = $response->getBody()->getContents();

        return [
            'http_code' => $statusCode,
            'result' => json_decode($body)
        ];
    }
}
