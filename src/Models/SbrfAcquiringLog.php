<?php

namespace Itgro\SbrfAcquiring\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class SbrfAcquiringLog
 * @package Itgro\SbrfAcquiring\Models
 *
 * @property string $type
 * @property string $method
 * @property string $request
 * @property string $response
 * @property string $message
 * @property string $stacktrace
 */
class SbrfAcquiringLog extends Model
{
    const LOG_TYPE_INFO = 'info';

    const LOG_TYPE_WARN = 'warn';

    const LOG_TYPE_ERROR = 'error';

    protected $casts = [
        'request' => 'array',
        'response' => 'array',
    ];
}
