<?php

namespace Itgro\SbrfAcquiring\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Itgro\SbrfAcquiring\Interfaces\OrderStatuses;

/**
 * @property mixed $order_id
 * @property string $external_id
 * @property Carbon $received_at
 * @property integer $amount
 * @property string $status
 * @property mixed $metadata
 * @property Carbon $created_at
 * @property Carbon $updated_at
 */
class SbrfAcquiringOrder extends Model implements OrderStatuses
{
    protected $casts = [
        'metadata' => 'array'
    ];

    public function setStatusAttribute($status)
    {
        if (array_has(self::STATUS_BY_INTEGER, $status)) {
            $status = $this->getStatusByIndex($status);
        }

        $this->attributes['status'] = $status;
    }

    public function getStatusByIndex($index)
    {
        return array_get(self::STATUS_BY_INTEGER, $index, OrderStatuses::STATUS_UNDEFINED);
    }
}
