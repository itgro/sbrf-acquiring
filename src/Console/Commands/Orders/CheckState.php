<?php

namespace Itgro\SbrfAcquiring\Console\Commands\Orders;

use Illuminate\Console\Command as Base;
use Itgro\SbrfAcquiring\Facades\ClientFacade as Client;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringOrder;

class CheckState extends Base
{
    public $signature = 'sbrf-acquiring:orders:check-state {order_id? : номер платежа}';

    public $description = 'Проверяет состояние платежа';

    public function handle()
    {
        $order_id = $this->argument('order_id');

        if ($order_id) {
            /** @var SbrfAcquiringOrder $order */
            $order = SbrfAcquiringOrder::find($order_id);

            $this->checkState($order);
        } else {
            $orders = SbrfAcquiringOrder::whereIn('status', [
                SbrfAcquiringOrder::STATUS_CREATED,
            ])->get();

            $orders->each(function (SbrfAcquiringOrder $order) {
                $this->checkState($order);
            });
        }
    }

    private function checkState(SbrfAcquiringOrder $order)
    {
        $result = Client::getStateOrder($order->external_id);

        $order->status = strtolower($result->OrderStatus);

        $order->save();
    }
}
