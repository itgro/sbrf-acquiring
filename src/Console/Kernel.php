<?php

namespace Itgro\SbrfAcquiring\Console;

use App\Console\Kernel as AppKernel;
use Illuminate\Console\Scheduling\Schedule;
use Itgro\SbrfAcquiring\Console\Commands\Orders\CheckState as OrderCheckState;

class Kernel extends AppKernel
{
    protected function schedule(Schedule $schedule)
    {
        parent::schedule($schedule);

        if (config('sbrf-acquiring.console.scheduling')) {
            $schedule->command(OrderCheckState::class)->everyFiveMinutes();
        }
    }
}
