<?php

namespace Itgro\SbrfAcquiring;

use GuzzleHttp\Client as GuzzleClient;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Itgro\SbrfAcquiring\Client\Client;
use Itgro\SbrfAcquiring\Client\GuzzleAdapter;
use Itgro\SbrfAcquiring\Console\Commands\Orders\{CheckState as OrderCheckStateCommand};
use Itgro\SbrfAcquiring\Console\Kernel as ConsoleKernel;
use Itgro\SbrfAcquiring\Controllers\Http\Pages;
use Itgro\SbrfAcquiring\Interfaces\Gate;

class ServiceProvider extends BaseServiceProvider
{
    public function boot()
    {
        $this->publishConfig();
        $this->publishMigrations();
        $this->publishViews();
        $this->registerConsoleCommands();
    }

    public function register()
    {
        $this->registerHelpers();
        $this->registerConfig();
        $this->registerRoutes();
        $this->registerViews();
        $this->registerClient();
        $this->registerClientFacade();
    }

    public function registerClientFacade()
    {
        $this->app->bind('sbrf-acquiring.facade.client', function () {
            return resolve(Client::class);
        });
    }

    public function registerHelpers()
    {
        require_once __DIR__ . '/helpers.php';
    }

    public function getGateUrl()
    {
        return config('sbrf-acquiring.test', true) ? Gate::API_URL_TEST : Gate::API_URL_PROD;
    }

    private function registerConsoleCommands()
    {
        $this->app->singleton('sbrf-acquiring.console.kernel', function ($app) {
            $dispatcher = $app->make(\Illuminate\Contracts\Events\Dispatcher::class);

            return new ConsoleKernel($app, $dispatcher);
        });

        $this->app->make('sbrf-acquiring.console.kernel');

        $this->commands([
            OrderCheckStateCommand::class,
        ]);
    }

    private function registerClient()
    {
        $this->app->bind(Client::class, function () {
            $adapter = new GuzzleAdapter(new GuzzleClient([
                'base_uri' => $this->getGateUrl(),
            ]));

            $login = config('sbrf-acquiring.login');
            $passw = config('sbrf-acquiring.password');

            return new Client($adapter, $login, $passw);
        });
    }

    private function registerConfig()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/sbrf-acquiring.php',
            'sbrf-acquiring'
        );
    }

    private function registerRoutes()
    {
        require __DIR__ . '/../routes/web.php';

        $this->app->make(Pages::class);
    }

    private function registerViews()
    {
        $this->loadViewsFrom(__DIR__ . '/../views', 'sbrf-acquiring');
    }

    private function publishConfig()
    {
        $this->publishes([
            __DIR__ . '/../config/sbrf-acquiring.php' => config_path('sbrf-acquiring.php'),
        ], 'config');
    }

    private function publishMigrations()
    {
        $timestamp = date('Y_m_d_His', time());

        if (!class_exists('CreateSbrfAcquiringOrdersTable')) {
            $this->publishes([
                __DIR__ . '/../database/migrations/create_sbrf_acquiring_orders_table.php' => $this->app->databasePath() . "/migrations/{$timestamp}_create_sbrf_acquiring_orders_table.php",
            ], 'migrations');
        }

        if (!class_exists('CreateSbrfAcquiringLogsTable')) {
            $this->publishes([
                __DIR__ . '/../database/migrations/create_sbrf_acquiring_logs_table.php' => $this->app->databasePath() . "/migrations/{$timestamp}_create_sbrf_acquiring_logs_table.php",
            ], 'migrations');
        }
    }

    private function publishViews()
    {
        $this->publishes([
            __DIR__ . '/../views' => resource_path('views/vendor/sbrf-acquiring'),
        ], 'views');
    }
}
