<?php

namespace Itgro\SbrfAcquiring\Facades;

use Illuminate\Support\Facades\Facade;

class ClientFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'sbrf-acquiring.facade.client';
    }

}
