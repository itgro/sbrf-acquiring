<?php

use Itgro\SbrfAcquiring\Models\SbrfAcquiringLog;

if (!function_exists('sbrf_write_log')) {
    function sbrf_write_log($type, $message, $stacktrace = null)
    {
        SbrfAcquiringLog::forceCreate([
            'type' => $type,
            'message' => $message,
            'stacktrace' => $stacktrace,
        ]);
    }
}
