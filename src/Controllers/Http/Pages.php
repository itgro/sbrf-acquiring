<?php

namespace Itgro\SbrfAcquiring\Controllers\Http;

use Artisan;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Itgro\SbrfAcquiring\Events\Callback as CallbackEvent;
use Itgro\SbrfAcquiring\Facades\ClientFacade;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringLog;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringOrder;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class Pages extends BaseController
{

    public function payment(Request $request)
    {
        $amount = $request->input('amount');

        if (!$amount) {
            return redirect()->back()->withErrors([
                'amount_error' => 'Укажите сумму платежа',
            ]);
        }

        $order_id = $request->input('order_id', null);

        $parameters = $request->input('parameters', []);

        $metadata = $request->input('metadata', []);

        $result = config('sbrf-acquiring.preauth_payment') ?
            ClientFacade::registerPreauthOrder($amount, $order_id, $parameters, $metadata) :
            ClientFacade::registerOrder($amount, $order_id, $parameters, $metadata);

        if ($result) {
            return redirect()->to($result->formUrl);
        }

        return redirect()->route(config('sbrf-acquiring.routes.fail'))->with('error', $result->errorMessage);
    }

    public function success(Request $request)
    {
        $external_id = $request->input('orderId');

        if ($external_id) {
            $order = SbrfAcquiringOrder::where('external_id', $external_id)->first();

            if ($order) {
                $this->artisanCall($order->id);
            }
        }

        return view('sbrf-acquiring::pages.success');
    }

    public function fail()
    {
        return view('sbrf-acquiring::pages.fail');
    }

    public function callback(Request $request)
    {
        $operation = $request->input('operation');

        $external_id = $request->input('mdOrder');

        $log = new SbrfAcquiringLog;

        $log->method = 'callbackPayment.' . $operation;
        $log->request = $request->all();

        if (!$external_id) {
            throw new InvalidParameterException();
        }

        $order = SbrfAcquiringOrder::where('external_id', $external_id)->first();

        event(new CallbackEvent($order, $operation));

        if (!$order) {
            $message = sprintf('Платеж %s не зарегистрирован в системе.', $external_id);

            $log->type = SbrfAcquiringLog::LOG_TYPE_WARN;
            $log->message = $message;

            $log->save();

            return 'OK';
        }

        switch ($operation) {
            case SbrfAcquiringOrder::STATUS_DEPOSITED:
            case SbrfAcquiringOrder::STATUS_APPROVED:
            case SbrfAcquiringOrder::STATUS_REFUNDED:
            case SbrfAcquiringOrder::STATUS_REVERSED:
            default:
                $this->artisanCall($order->id);
                break;
        }

        $log->type = SbrfAcquiringLog::LOG_TYPE_INFO;
        $log->response = 'ОК';

        $log->save();

        return 'OK';
    }

    private function artisanCall($order_id)
    {
        Artisan::call('sbrf-acquiring:orders:check-state', [
            'order_id' => $order_id,
        ]);
    }
}