<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Itgro\SbrfAcquiring\Models\SbrfAcquiringLog;

class CreateSbrfAcquiringLogsTable extends Migration
{
    public function up()
    {
        Schema::create('sbrf_acquiring_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type', [
                SbrfAcquiringLog::LOG_TYPE_INFO,
                SbrfAcquiringLog::LOG_TYPE_WARN,
                SbrfAcquiringLog::LOG_TYPE_ERROR,
            ])->nullable();
            $table->string('method')->nullable();
            $table->text('request')->nullable();
            $table->text('response')->nullable();
            $table->text('message')->nullable();
            $table->text('stacktrace')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sbrf_acquiring_logs');
    }
}
