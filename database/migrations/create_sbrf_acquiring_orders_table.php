<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Itgro\SbrfAcquiring\Interfaces\OrderStatuses;

class CreateSbrfAcquiringOrdersTable extends Migration
{
    public function up()
    {
        Schema::create('sbrf_acquiring_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id');
            $table->string('external_id');
            $table->integer('amount');
            $table->text('metadata');
            $table->dateTime('received_at')->nullable();

            $table->enum('status', array_merge([
                OrderStatuses::STATUS_NEW,
                OrderStatuses::STATUS_UNDEFINED,
            ], OrderStatuses::STATUS_BY_INTEGER))
                ->default(OrderStatuses::STATUS_NEW);

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('sbrf_acquiring_orders');
    }
}
